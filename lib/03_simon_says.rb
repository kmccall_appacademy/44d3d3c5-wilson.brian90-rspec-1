
def echo(word)
  return word
end

def shout(word)
  return word.upcase
end

def repeat(word, num=2)
  array = []
  num.times do
    array << word
  end
  array.join(" ")

end

def start_of_word(word, idx)
  word.chars[0..idx-1].join
end

def first_word(array)
  array.split[0]
end

def titleize(string)
  little_words = ["and", "the", "over"]
  array = string.split
  first_word = array[0].chars
  first_word[0] = first_word[0].upcase

  fixed = array[1..array.length - 1].map {|word|
    if !little_words.include?(word)
      temp = word.chars
      temp[0] = temp[0].upcase
      word = temp.join
    end
    word = word
  }
  fixed.unshift(first_word.join)
  fixed.join(" ")
end
