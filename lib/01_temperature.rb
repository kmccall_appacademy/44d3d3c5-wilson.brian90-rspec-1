def ftoc(fahrenheit)
  ((fahrenheit.to_f - 32) * 5 / 9).to_i
end

def ctof(celsius)
  ((celsius.to_f * 9 / 5) + 32).to_f
end
