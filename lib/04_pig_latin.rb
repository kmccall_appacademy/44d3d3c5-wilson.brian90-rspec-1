def translate(string)
  array = string.split
  vowels = ["a", "e", "i", "o", "u"]

  array = array.map do |word|
    split = word.chars
    fragment = nil

    #get the first fragment, whether single consonant or more
    if split[0..2].all?{|char| !vowels.include?(char)} || split[1..2] == ["q", "u"]
      fragment = split[0..2]
      split[0..2] = nil
      split.compact!
    end

    if split[0..1].all?{|char| !vowels.include?(char)} || split[0..1] == ["q", "u"]
      fragment = split[0..1]
      split[0..1] = nil
      split.compact!
    end

    if !vowels.include?(split[0])
      fragment = split[0]
      split.delete_at(0)
    end

    flag = true
    while flag do
      if !vowels.include?(split[0])
        puts "before" + split.to_s
        split.shift
        puts "after" + split.to_s + "\n\n"
      end
      flag = false if vowels.include?(split[0])
    end

    if fragment != nil
      split << fragment
    end
    split << "a"
    split << "y"
    word = split.join

  end

  array.join(" ")

end
