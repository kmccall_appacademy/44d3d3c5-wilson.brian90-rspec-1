def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(array)
  if array.size == 0
    return 0
  end
  array.inject(:+)
end

def multiply(array)
  array.inject(:*)
end

def power(a, b)
  a ** b
end

def factorial(x)
  if x == 0
    return 0
  end

  (1..x).to_a.inject(:*)
end
